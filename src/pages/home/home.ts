import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { NavController } from 'ionic-angular';
import { Subject }    from 'rxjs/Subject';
import { Subscription }    from 'rxjs/Subscription';
import { interval } from 'rxjs/observable/interval';
import { BatteryStatus } from '@ionic-native/battery-status';
import { DeviceMotion, DeviceMotionAccelerationData } from '@ionic-native/device-motion';
import { Platform } from 'ionic-angular';

import 'rxjs/add/operator/debounceTime';

const BASE_URL: string ="http://witekiot.westeurope.cloudapp.azure.com:8080/api/v1/WzpG6WuyNQF2AHgYQvya/telemetry" ;
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit, OnDestroy{
  temp : number = 22;
  humidity : number = 6;

  private temp$: Subject<number>;
  private humidity$: Subject<number>;

  private tempSub : Subscription;
  private humiditySub : Subscription;
  private defaultSub : Subscription;
  private batterySub : Subscription;
  private accelerationSub : Subscription;

  constructor(public navCtrl: NavController, private http: HttpClient, private batteryStatus: BatteryStatus, private deviceMotion: DeviceMotion, private platform: Platform) {
    this.temp$ = new Subject<number>();
    this.humidity$ = new Subject<number>();
  }

  ngOnInit(){
    this.tempSub = this.temp$.subscribe( next => {
      console.log("temp", next);
      this.temp = next;
      this.pushDataToThingsBoard({
        temperature : next
      })
    });

    this.humiditySub = this.humidity$.subscribe( next => {
      console.log("humidity", next);
      this.humidity = next;
      this.pushDataToThingsBoard({
        humidity : next
      })
    });

    this.defaultSub = interval(2000).subscribe( next => {
      this.pushDataToThingsBoard({
        temperature : this.temp,
        humidity : this.humidity
      })
    });

    // watch change in battery status
    this.batterySub = this.batteryStatus.onChange().subscribe(status => {
      console.log(status.level, status.isPlugged);
      this.pushDataToThingsBoard({
        battery : status.level
      })
    });

    if (this.platform.is('ios') || this.platform.is('android')) {
      this.accelerationSub  = this.deviceMotion.watchAcceleration({frequency:1000}).subscribe((acceleration: DeviceMotionAccelerationData) => {
        console.log(acceleration);
        this.pushDataToThingsBoard({
          accX : acceleration.x.toFixed(2),
          accY : acceleration.y.toFixed(2),
          accZ : acceleration.z.toFixed(2)
        })
      });
    }
  }

  ngOnDestroy(){
    this.tempSub.unsubscribe();
    this.humiditySub.unsubscribe();
    this.defaultSub.unsubscribe();
    this.batterySub.unsubscribe();
    if (this.platform.is('ios') || this.platform.is('android')) {
      this.accelerationSub.unsubscribe();
    }
  }

  onChangeTemp(event) {
    this.temp$.next(event._value);
  }

  onChangeHumidity(event) {
    this.humidity$.next(event._value);
  }

  private pushDataToThingsBoard(data:any): void {
    this.http.post(`${BASE_URL}`, data).subscribe();
  }
  
}
